import ordersSagas from 'containers/HomePage/sagas';

export default function* rootSaga() {
  yield [
    ...ordersSagas,
  ];
}
