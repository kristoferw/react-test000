import { combineReducers } from 'redux-immutable';
import ordersReducer from 'containers/HomePage/reducer';

export default function createReducer() {
  return combineReducers({
    orders: ordersReducer,
  });
}
