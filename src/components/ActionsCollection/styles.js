const styles = {
  lightIcon: {
    color: '#e0f2f1',
    margin: '0 5px',
    cursor: 'pointer',
  },
  darkIcon: {
    color: '#697a8d',
    margin: '0 10px',
    cursor: 'pointer',
  },
};

export default styles;
