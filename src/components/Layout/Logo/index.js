import React, { PropTypes } from 'react';
import { Link } from 'react-router-dom';
import LogoSVG from './outvio-logo.svg';

import styles from './styles';

function Logo({ onRequestChange }) {
  return (
    <Link to="/" style={styles.logoWrapper} onTouchTap={() => onRequestChange(false)}>
      <LogoSVG height={35} width={170} />
    </Link>
  );
}

Logo.propTypes = {
  onRequestChange: PropTypes.func,
};

Logo.defaultProps = {
  onRequestChange: () => {},
};

export default Logo;
