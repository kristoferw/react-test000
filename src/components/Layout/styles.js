const styles = {
  containerWrapper: {
    display: 'flex',
    bottom: 0,
    position: 'absolute',
    width: '100%',
    top: 64,
  },
  contentWrapper: {
    backgroundColor: '#e0f2f1',
    padding: 3,
    flex: 1,
    overflow: 'auto',
  },
};

export default styles;
