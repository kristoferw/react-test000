const styles = {
  linkStyle: {
    textDecoration: 'none',
  },
  wrapperStyle: {
    height: '100%',
  },
  mobileOverlayStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  desktopContainerStyle: {
    position: 'relative',
  },
  logoContainerStyle: {
    height: 64,
    paddingLeft: 15,
    backgroundColor: '#374a55',
  },
  menuItem: {
    paddingLeft: 60,
    color: '#a8b7c5',
    fontSize: 20,
    fontWeight: 300,
  },
  menuItemActive: {
    backgroundColor: '#cddc39',
    color: '#374a54',
  },
  menuItemIcon: {
    fontSize: 25,
    color: '#a8b7c5',
    position: 'absolute',
    left: -20,
    lineHeight: '50px',
  },
  menuItemIconActive: {
    color: '#374a54',
  },
};

export default styles;
