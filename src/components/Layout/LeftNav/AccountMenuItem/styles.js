const styles = {
  containerStyle: {
    backgroundColor: '#3d727c',
    padding: '20px 20px 40px 20px',
  },
  avatarCircle: {
    backgroundColor: '#e0f2f1',
    width: 50,
    height: 50,
    textAlign: 'center',
    color: '#3d727c',
    fontSize: 20,
    lineHeight: '50px',
    display: 'inline-block',
    marginRight: 10,
    verticalAlign: 'top',
  },
  textWrapper: {
    display: 'inline-block',
    color: '#e0f2f1',
    fontSize: 15,
    lineHeight: '25px',
    paddingTop: 12,
    verticalAlign: 'top',
    fontWeight: 300,
    cursor: 'pointer',
  },
  dropDownIcon: {
    color: '#e0f2f1',
    position: 'absolute',
    verticalAlign: 'top',
    fontSize: 30,
    lineHeight: '25px',
  }
};

export default styles;
