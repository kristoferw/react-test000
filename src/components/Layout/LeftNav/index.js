import React, { PropTypes } from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import { Drawer, MenuItem } from 'material-ui';
import { Responsive } from 'components';
import AccountMenuItem from './AccountMenuItem';
import FontIcon from 'material-ui/FontIcon';
import Logo from '../Logo';

import styles from './styles';

function LeftNav({ open, onRequestChange, menuItems, personalInfo }) {
  const accountItemContainer = <AccountMenuItem personalInfo={personalInfo} />;
  const menuItemsContainer = menuItems.map(item =>
    <Route path={item.path} key={item.path}>
      <div>
        {
          menuItems.map(menuItem => {
            const active = (menuItem.path === item.path);
            return (
              <Link to={menuItem.path} key={menuItem.path} style={styles.linkStyle}>
                <MenuItem
                  key={menuItem.path}
                  style={{ ...styles.menuItem, ...(active && styles.menuItemActive) }}
                  onTouchTap={() => onRequestChange(false)}
                >
                  <FontIcon
                    className="material-icons"
                    style={{ ...styles.menuItemIcon, ...(active && styles.menuItemIconActive) }}
                  >
                    {menuItem.icon}
                  </FontIcon>
                  {menuItem.label}
                </MenuItem>
              </Link>
            );
          })
        }
      </div>
    </Route>
  );
  const itemsContainer = (
    <div>
      {accountItemContainer}
      <Switch>
        {menuItemsContainer}
      </Switch>
    </div>
  );
  return (
    <div style={styles.wrapperStyle}>
      <Responsive size="desktop">
        <Drawer
          open={true}
          style={styles.wrapperStyle}
          containerStyle={styles.desktopContainerStyle}
        >
          {itemsContainer}
        </Drawer>
      </Responsive>
      <Responsive size="mobile">
        <Drawer
          open={open}
          docked={false}
          overlayStyle={styles.mobileOverlayStyle}
          onRequestChange={onRequestChange}
        >
          <div style={styles.logoContainerStyle}><Logo onRequestChange={onRequestChange} /></div>
          {itemsContainer}
        </Drawer>
      </Responsive>
    </div>
  );
}

LeftNav.propTypes = {
  open: PropTypes.bool,
  onRequestChange: PropTypes.func,
  personalInfo: PropTypes.shape(),
  menuItems: PropTypes.arrayOf(PropTypes.shape()),
};

LeftNav.defaultProps = {
  open: false,
  onRequestChange: () => {},
  menuItems: [
    { label: 'Orders', path: '/orders', icon: 'local_grocery_store' },
    { label: 'Shipments', path: '/shipments', icon: 'local_shipping' },
    { label: 'Returns', path: '/returns', icon: 'keyboard_return' },
    { label: 'Reports', path: '/reports', icon: 'view_quilt' },
    { label: 'Account', path: '/account', icon: 'settings' },
  ],
  personalInfo: {
    firstName: 'John',
    lastName: 'Smith',
    website: 'Breastfeeling.com',
  },
};

export default LeftNav;
