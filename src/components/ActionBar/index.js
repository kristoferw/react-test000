import React, { PropTypes } from 'react';
import { FontIcon, Paper } from 'material-ui';
import { FiltersCollection, ActionsCollection } from 'components';

import styles from './styles';

function ActionBar({ actions }) {
  return (
    <Paper zDepth={1} style={styles.container}>
      <FiltersCollection />
      <div style={styles.actionsCollection}><ActionsCollection actions={actions} brightness="dark" /></div>
    </Paper>
  );
}

ActionBar.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.string,
    dropdownItems: PropTypes.arrayOf(PropTypes.shape({
      label: PropTypes.string,
      action: PropTypes.func,
    })),
    action: PropTypes.func,
  })),
};

ActionBar.defaultProps = {
  actions: [],
};

export default ActionBar;
