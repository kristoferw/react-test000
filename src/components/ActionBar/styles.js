const styles = {
  container: {
    padding: 10,
    margin: 3,
    display: 'flex',
    color: '#697a8d',
    lineHeight: '27px',
  },
  actionsCollection: {
    flex: '1',
    textAlign: 'right',
  },
};

export default styles;
