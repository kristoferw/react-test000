const styles = {
  container: {
    padding: 10,
    margin: 3,
    display: 'flex',
    color: '#697a8d',
    lineHeight: '27px',
  },
  checkboxWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  checkbox: {
    width: 'auto',
    verticalAlign: 'middle',
  },
  checkboxIcon: {
    fill: '#697a8d',
  },
  content: {
    flex: 1,
  },
  topPart: {
    display: 'flex',
    fontWeight: 'bold',
  },
  bottomPart: {
    display: 'flex',
  },
  rightPart: {
    flex: 1,
    textAlign: 'right',
  },
  marginLeft: {
    marginLeft: 7,
    lineHeight: '27px',
    height: 27,
    position: 'relative',
    display: 'inline-block',
    verticalAlign: 'middle',
  },
  icon: {
    color: '#697a8d',
  }
};

export default styles;
