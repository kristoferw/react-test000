import React, { PropTypes } from 'react';
import moment from 'moment';
import { formatMoney } from 'accounting';
import { getSymbolFromCurrency } from 'currency-symbol-map';
import { Paper, Checkbox, FontIcon } from 'material-ui';

import styles from './styles';

function OrderItem({
  orderInfo: {
    id,
    createdAt,
    amount,
    currency,
    customer: {
      email,
    },
    shipment,
    items,
  },
}) {
  const date = new Date(createdAt);
  const iconShipment = shipment.type === 'standard' ? 'directions_car' : 'flight';
  return (
    <Paper zDepth={1} style={styles.container}>
      <div style={styles.checkboxWrapper}>
        <Checkbox style={styles.checkbox} iconStyle={styles.checkboxIcon} />
      </div>
      <div style={styles.content}>
        <div style={styles.topPart}>
          <div style={styles.leftPart}>{id}</div>
          <div style={styles.rightPart}>
            {email}
          </div>
        </div>
        <div style={styles.bottomPart}>
          <div style={styles.leftPart}>{moment(createdAt).format('DD/MM/YY H:mm')}</div>
          <div style={styles.rightPart}>
            <span style={styles.marginLeft}>{shipment.from.country}</span>
            <span style={styles.marginLeft}>
              <FontIcon className="material-icons" style={styles.icon}>
                {iconShipment}
              </FontIcon>
            </span>
            <span style={styles.marginLeft}>
              <FontIcon className="material-icons" style={styles.icon}>
                credit_card
              </FontIcon>
            </span>
            <span style={styles.marginLeft}>{items.length}</span>
            <span style={styles.marginLeft}>{formatMoney(amount, getSymbolFromCurrency(currency))}</span>
          </div>
        </div>
      </div>
    </Paper>
  );
}

OrderItem.propTypes = {
  orderInfo: PropTypes.shape({
    id: PropTypes.number,
    createdAt: PropTypes.string,
    amount: PropTypes.string,
    currency: PropTypes.string,
    customer: PropTypes.shape({
      email: PropTypes.string,
    }),
    shipment: PropTypes.shape({
      type: PropTypes.string,
      from: PropTypes.shape({
        country: PropTypes.string,
      }),
      to: PropTypes.shape({
        country: PropTypes.string,
      }),
    }),
    items: PropTypes.arrayOf(PropTypes.shape()),
  }),
};

OrderItem.defaultProps = {
  orderInfo: {
    id: 0,
    createdAt: '',
    amount: '0.00',
    currency: 'USD',
    customer: {
      email: '',
    },
    shipment: {
      type: 'standard',
      from: {
        country: '',
      },
      to: {
        country: '',
      },
    },
    items: [],
  },
};

export default OrderItem;
