import { delay } from 'redux-saga';
import { put, call, takeEvery } from 'redux-saga/effects';

import {
  FETCH_ORDERS_START,
} from './constants';

import {
  ordersFetched,
} from './actions';

import { getOrderItems } from './api';

function* asyncFetchOrders() {
  const response = yield call(getOrderItems);
  yield put(ordersFetched(response));
}

export function* sagaWatcher() {
  yield takeEvery(FETCH_ORDERS_START, asyncFetchOrders);
}

export default [
  sagaWatcher(),
];
