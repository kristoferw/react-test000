const styles = {
  container: {
    paddingBottom: 100,
  },
  addCircleButton: {
    position: 'absolute',
    right: 20,
    bottom: 20,
  },
};

export default styles;
