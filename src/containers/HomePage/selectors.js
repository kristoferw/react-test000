import { createSelector } from 'reselect';

const ordersSelector = state => state.get('orders');
const orderItemsSelector = createSelector([ordersSelector], orders => orders.get('orderItems'));

export {
  ordersSelector,
  orderItemsSelector,
};
