import { createAction } from 'redux-actions';
import {
  FETCH_ORDERS_START,
  FETCH_ORDERS_SUCCESS,
  FETCH_ORDERS_FAILURE,
} from './constants';

export const fetchOrders = createAction(FETCH_ORDERS_START);
export const ordersFetched = createAction(
  FETCH_ORDERS_SUCCESS,
  orderItems => ({ orderItems }),
);
