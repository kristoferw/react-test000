import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

import {
  FETCH_ORDERS_SUCCESS,
} from './constants';

const initialState = Map({
  orderItems: [],
});

export default handleActions({
  [FETCH_ORDERS_SUCCESS]: (state, action) =>
    state
      .set('orderItems', action.payload.orderItems),
}, initialState);
