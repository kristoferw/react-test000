import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { RaisedButton } from 'material-ui';

import { ActionBar, Layout, OrderItem, AddCircleButton } from 'components';
import { orderItemsSelector } from './selectors';
import { fetchOrders } from './actions';

import styles from './styles';

const mapStateToProps = state => ({
  orderItems: orderItemsSelector(state),
});

const mapDispatchToProps = dispatch => ({
  fetchOrders: () => dispatch(fetchOrders()),
});

class HomePage extends React.Component {
  componentDidMount() {
    this.props.fetchOrders();
  }

  render() {
    const { orderItems } = this.props;
    const actions = [
      { icon: 'refresh' },
      { icon: 'tune' },
      {
        icon: 'more_vert',
        dropdownItems: [
          { label: 'Select All' },
          { label: 'Upload CSV...' },
        ],
      },
    ];
    return (
      <div style={styles.container}>
        <ActionBar actions={actions} />
        {
          orderItems.map(orderItem =>
            <OrderItem orderInfo={orderItem} key={orderItem.id} />
          )
        }
        <div style={styles.addCircleButton}>
          <AddCircleButton />
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
