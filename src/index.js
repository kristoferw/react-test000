import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { fromJS } from 'immutable';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';

import Router from 'setup/Router';
import configureStore from 'setup/store';

injectTapEventPlugin();

const muiTheme = getMuiTheme({
  appBar: {
    color: '#374a55',
  },
  drawer: {
    color: '#00796b',
  }
});
console.log(muiTheme);

const initialState = fromJS({});
const store = configureStore(initialState);

function renderApp(Component) {
  render(
    <AppContainer>
      <MuiThemeProvider muiTheme={muiTheme}>
        <Provider store={store}>
          <Component />
        </Provider>
      </MuiThemeProvider>
    </AppContainer>,
    document.getElementById('root'),
  );
}

renderApp(Router);

if (module.hot) {
  module.hot.accept('./setup/Router/index.js', () => {
    const NewApp = require('./setup/Router/index.js').default;
    renderApp(NewApp);
  });
}
